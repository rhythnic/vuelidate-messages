type VuelidateField = any
type Vue = any

type ValidationMessageGenerator = (field: VuelidateField, ...rest: any[]) => string

interface ValidationMessages {
  [propName: string]: ValidationMessageGenerator
}

interface VuelidateMessageOptions {
  dirty?: boolean
  first?: number
}

const defaultOptions: VuelidateMessageOptions = {
  dirty: true,
  first: 1
}

export function validationMessages (
  messages: ValidationMessages,
  opts: VuelidateMessageOptions
): (field: VuelidateField, ...args: any[]) => string[] {
  return function (this: Vue, field, ...args) {
    opts = { ...defaultOptions, ...opts }
    if (field.$pending || (opts.dirty && !field.$dirty)) return []
    const isInvalid = (x: string) => x.charAt(0) !== '$' && field[x] === false
    const keys = Object.keys(field).filter(isInvalid).slice(0, opts.first)
    return keys.map(x => {
      if (!messages[x]) return 'Invalid'
      return messages[x].call(this, field, ...args)
    })
  }
}

export function validationMessage (
  messages: ValidationMessages,
  opts: VuelidateMessageOptions
): (field: VuelidateField, ...args: any[]) => string {
  const mkMsgs = validationMessages(messages, { ...opts, first: 1 })
  return function (this: Vue, ...args) {
    return mkMsgs.apply(this, args)[0] || ''
  }
}
